FROM microsoft/dotnet:2.1-aspnetcore-runtime AS base
RUN apt-get install --yes curl
RUN curl --silent --location https://deb.nodesource.com/setup_10.x | bash -
RUN apt-get install --yes nodejs
WORKDIR /app
EXPOSE 443
EXPOSE 80

FROM mcr.microsoft.com/dotnet/core/sdk:2.2 AS build
ENV BuildingDocker true
WORKDIR /src
COPY ./Carsales.sln  ./
COPY ./Web/Web.csproj ./Web/Web.csproj
COPY ./Data/Data.csproj ./Data/Data.csproj
RUN dotnet restore 
COPY ./Data/. ./Data/
COPY ./Web/. ./Web/
WORKDIR /src/Web
RUN dotnet publish -c Release -o out

FROM node:10-alpine as build-node
WORKDIR /ClientApp
COPY ./Web/ClientApp/package.json .
COPY ./Web/ClientApp/package-lock.json .
RUN npm install
COPY ./Web/ClientApp/ . 
RUN npm run build

FROM base AS runtime
WORKDIR /app
COPY --from=build /src/Web/out ./
COPY --from=build-node /ClientApp/build ./ClientApp/build
ENTRYPOINT ["dotnet", "Web.dll"]