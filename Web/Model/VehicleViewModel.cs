﻿using System;
using Data.Entity;

namespace Web.Model
{
    public abstract class VehicleViewModel
    {
        public Guid Id { get; set; }

        public string Make { get; set; }

        public string Model { get; set; }

        public Vehicle.VehicleType Type { get; set; }

    }
}