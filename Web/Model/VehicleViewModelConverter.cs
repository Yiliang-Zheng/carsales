﻿using System;
using Data.Entity;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Web.Model
{
    public class VehicleViewModelConverter : JsonConverter
    {
        public override bool CanWrite => false;

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            // Load JObject from stream
            var jObject = JObject.Load(reader);

            // Create target object based on JObject
            VehicleViewModel target;
            switch (jObject.Value<int>(nameof(VehicleViewModel.Type)))
            {
                case (int)Vehicle.VehicleType.Car:
                    target = new CarViewModel();
                    break;
                default:
                    target = new CarViewModel();
                    break;
            }

            // Populate the object properties
            serializer.Populate(jObject.CreateReader(), target);

            return target;
        }

        public override bool CanConvert(Type objectType)
        {
            return typeof(VehicleViewModel).IsAssignableFrom(objectType);
        }
    }
}