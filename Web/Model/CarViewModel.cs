﻿using FluentValidation;

namespace Web.Model
{
    public class CarViewModel : VehicleViewModel
    {

        public string Engine { get; set; }

        public int Doors { get; set; }

        public int Wheels { get; set; }

        public int BodyType { get; set; }
    }

    public class CarViewModelValidator : AbstractValidator<CarViewModel>
    {
        public CarViewModelValidator()
        {
            RuleFor(p => p.Make).NotEmpty();
            RuleFor(p => p.Model).NotEmpty();
            RuleFor(p => p.Engine).NotEmpty();
            RuleFor(p => p.Wheels).GreaterThanOrEqualTo(4);
            RuleFor(p => p.Doors).GreaterThanOrEqualTo(3);
        }
    }
}