﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Web.Model;
using Web.Services;

namespace Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehicleController : ControllerBase
    {
        private readonly VehicleFactory _factory;

        public VehicleController(VehicleFactory factory)
        {
            _factory = factory;
        }

        [HttpGet("list/{type}")]
        public async Task<IActionResult> ListVehicles(string type)
        {
            try
            {
                var service = this._factory.CreateService(type);
                var result = await service.ListVehicles();
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost("create")]
        public async Task<IActionResult> CreateVehicle(VehicleViewModel model)
        {
            try
            {
                var type = model.Type.ToString();
                var service = this._factory.CreateService(type);
                await service.CreateVehicle(model);
                var result = await service.ListVehicles();
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}