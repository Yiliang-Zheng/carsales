﻿using AutoMapper;
using Data.Entity;
using Web.Model;

namespace Web
{
    public class AutoMapperProfile: Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<CarViewModel, Car>()
                .ForMember(p => p.BodyType, opts => opts.MapFrom(src => (Car.CarType) src.BodyType))
                .ForMember(dest => dest.Type, opts => opts.MapFrom(src => Vehicle.VehicleType.Car));

            CreateMap<Car, CarViewModel>()
                .ForMember(dest => dest.BodyType, opts => opts.MapFrom(src => (int) src.BodyType));
        }
    }
}