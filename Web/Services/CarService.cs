﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Data.Entity;
using Data.Repository;
using FluentValidation;
using Web.Model;

namespace Web.Services
{
    public class CarService : IVehicleService
    {
        private readonly IRepository<Vehicle> _repository;
        private readonly IMapper _mapper;
        private readonly IValidator<CarViewModel> _validator;

        public CarService(IRepository<Vehicle> repository, IMapper mapper, IValidator<CarViewModel> validator)
        {
            _repository = repository;
            this._mapper = mapper;
            this._validator = validator;
        }

        public async Task CreateVehicle(VehicleViewModel model)
        {
            this._validator.ValidateAndThrow((CarViewModel)
                model);
            model.Id = Guid.NewGuid();
            var car = this._mapper.Map<Car>(model);
            await this._repository.Insert(car);
        }

        public async Task<List<VehicleViewModel>> ListVehicles()
        {
            var result = new List<VehicleViewModel>();
            
            (await this._repository.ListBy(p=>p.Type == Vehicle.VehicleType.Car))
                .ForEach(p =>
            {
                var item = this._mapper.Map<CarViewModel>((Car)p);
                result.Add(item);
            });
            return result;
        }
    }
}