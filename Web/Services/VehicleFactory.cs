﻿using System;
using Microsoft.Extensions.DependencyInjection;

namespace Web.Services
{
    public class VehicleFactory
    {
        private readonly IServiceProvider _serviceProvider;

        public VehicleFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public IVehicleService CreateService(string type)
        {
            if (type.Equals("car", StringComparison.InvariantCultureIgnoreCase))
            {
                return this._serviceProvider.GetService<CarService>();
            }

            throw new Exception($"Invalid type. {type} is not supported yet.");
        }
    }
}