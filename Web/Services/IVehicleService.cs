﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Web.Model;

namespace Web.Services
{
    public interface IVehicleService
    {
        Task CreateVehicle(VehicleViewModel model);

        Task<List<VehicleViewModel>> ListVehicles();
    }
}