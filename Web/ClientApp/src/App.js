﻿import React from "react";
import { Route } from "react-router";
import Layout from "./components/Layout";
import CreateCarPage from "containers/CreateCarPage";
import ManageCarPage from "containers/ManageCarPage";

export default () => (
  <Layout>
    <Route exact path="/" component={ManageCarPage} />
    <Route path="/vehicle/create" component={CreateCarPage} />
  </Layout>
);
