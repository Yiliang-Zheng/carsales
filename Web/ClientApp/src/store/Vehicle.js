import { createReducer } from "utils/redux";
import types from "actions/types/vehicle";

const initState = {
  list: [],
};

const reducer = createReducer(initState, {
  [types.LOAD_VEHICLES_SUCCESS]: (state, list) => ({
    ...state,
    list,
  }),
});

export default reducer;
