export const VEHICLE_TYPE = {
  CAR: 0,
  BOAT: 1,
  BIKE: 2,
};

export const VEHICLE_BODY_TYPE = [
  {
    name: "Hatchback",
    id: 0,
  },
  {
    name: "Sedan",
    id: 1,
  },
  {
    name: "SUV",
    id: 2,
  },
];
