import { createConstants } from "utils/redux";

const types = createConstants({ prefix: "@vehicle/" }, "LOAD_VEHICLES_SUCCESS");

export default types;
