import { createAction } from "utils/redux";
import { get, post } from "utils/request";
import types from "actions/types/vehicle";

export function loadCars() {
  return function (dispatch) {
    return get("/api/vehicle/list/car").then((list) => {
      dispatch(createAction(types.LOAD_VEHICLES_SUCCESS, list));
    });
  };
}

export function createCar(car) {
  return function (dispatch) {
    return post("/api/vehicle/create", car).then((list) => {
      dispatch(createAction(types.LOAD_VEHICLES_SUCCESS, list));
    });
  };
}
