import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { Row, Col, PageHeader } from "antd";
import CreateCarForm from "components/CreateCarForm";

import { createCar, loadCars } from "actions/vehicle";

class CreateCarPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      saving: false,
    };
    this.handleCarCreate = this.handleCarCreate.bind(this);
  }

  async handleCarCreate(values) {
    try {
      this.setState({ saving: true });
      await this.props.createCar(values);
      this.setState({ saving: false });
    } catch (error) {
      this.setState({ saving: false });
      console.log(error);
      throw new Error(error);
    }
  }

  render() {
    return (
      <Row gutter={4}>
        <Col xs={{ offset: 0, span: 24 }} md={{ offset: 6, span: 12 }}>
          <PageHeader
            onBack={() => this.props.history.push("/")}
            title="Create Car"
            subTitle="Use this form to create a new car."
          />

          <div
            style={{
              margin: "1rem",
              padding: "1rem",
              backgroundColor: "#ffffff",
              borderRadius: "5px",
            }}
          >
            <CreateCarForm
              saving={this.state.saving}
              onCarCreate={this.handleCarCreate}
            />
          </div>
        </Col>
      </Row>
    );
  }
}

export default withRouter(
  connect(
    (state) => state.vehicle,
    (dispatch) => bindActionCreators({ loadCars, createCar }, dispatch)
  )(CreateCarPage)
);
