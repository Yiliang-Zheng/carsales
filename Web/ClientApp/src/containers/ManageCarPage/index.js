import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Route } from "react-router-dom";
import { Row, Col, PageHeader, Button } from "antd";
import CarList from "components/CarList";

import { loadCars } from "actions/vehicle";

class ManageCarPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
    };
  }

  async componentDidMount() {
    try {
      this.setState({ loading: true });
      await this.props.loadCars();
      this.setState({ loading: false });
    } catch (error) {
      console.log(error);
      this.setState({ loading: false });
    }
  }

  render() {
    return (
      <Row>
        <Col xs={{ offset: 0, span: 24 }} md={{ offset: 6, span: 12 }}>
          <PageHeader
            onBack={null}
            title="Car List"
            subTitle="View all cars"
            extra={[
              <Route
                key="create-car"
                render={({ history }) => (
                  <Button
                    type="primary"
                    htmlType="button"
                    onClick={() => {
                      history.push("/vehicle/create");
                    }}
                  >
                    Create Car
                  </Button>
                )}
              />,
            ]}
          />

          <div style={{ margin: "1rem" }}>
            <CarList
              loading={this.state.loading}
              cars={this.props.vehicle.list}
            />
          </div>
        </Col>
      </Row>
    );
  }
}

export default connect(
  (state) => ({ vehicle: state.vehicle }),
  (dispatch) => bindActionCreators({ loadCars }, dispatch)
)(ManageCarPage);
