import React from "react";
import { Route } from "react-router-dom";
import { Skeleton, Table, Tag, Empty, Button } from "antd";

import { VEHICLE_BODY_TYPE } from "utils/constant";
const CarList = ({ loading, cars }) => {
  const columns = [
    {
      title: "Make",
      dataIndex: "make",
      key: "make",
    },
    {
      title: "Model",
      dataIndex: "model",
      key: "model",
    },
    {
      title: "Engine",
      dataIndex: "engine",
      key: "engine",
    },
    {
      title: "Body Type",
      render: (text, record) => {
        var type = VEHICLE_BODY_TYPE.find((p) => p.id === record.bodyType);
        return <Tag color="#108ee9">{type.name}</Tag>;
      },
    },
  ];

  return (
    <Skeleton active title paragraph={{ rows: 10 }} loading={loading}>
      <Table
        rowKey={(p) => p.id}
        dataSource={cars}
        columns={columns}
        locale={{
          emptyText: (
            <Empty
              imageStyle={{
                height: 60,
              }}
              description={<span>No Car Yet</span>}
            >
              <Route
                render={({ history }) => (
                  <Button
                    type="primary"
                    onClick={() => {
                      history.push("/vehicle/create");
                    }}
                  >
                    Create Now
                  </Button>
                )}
              />
            </Empty>
          ),
        }}
      />
    </Skeleton>
  );
};

export default CarList;
