﻿import React from "react";
import { Link } from "react-router-dom";
import "./NavMenu.css";

import { Menu } from "antd";
// import styles from "./styles.module.scss";
export default (props) => (
  <React.Fragment>
    <Menu mode="horizontal" theme="dark">
      <Menu.Item key="home">
        <Link to="/"> Manage Car</Link>
      </Menu.Item>
    </Menu>
  </React.Fragment>
);
