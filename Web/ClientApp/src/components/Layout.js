import React from "react";
import NavMenu from "./NavMenu";

import { Layout, Col, Row } from "antd";

export default (props) => (
  <Layout style={{ minHeight: "100vh" }}>
    <Layout.Header>
      <div className="logo">
        <a href="/">
          <img src={`${process.env.PUBLIC_URL}/carsales-white.svg`} />
        </a>
      </div>
      <NavMenu />
    </Layout.Header>
    <Layout.Content>
      <Row>
        <Col xs={{ offset: 0, span: 24 }} md={{ offset: 3, span: 18 }}>
          {props.children}
        </Col>
      </Row>
    </Layout.Content>
    <Layout.Footer></Layout.Footer>
  </Layout>
);
