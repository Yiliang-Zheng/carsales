import React from "react";
import { Form, Input, InputNumber, Select, Button, message } from "antd";
import { VEHICLE_TYPE } from "utils/constant";

const CreateCarForm = ({ saving, onCarCreate }) => {
  const [form] = Form.useForm();
  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 6 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16 },
    },
  };
  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 16,
        offset: 6,
      },
    },
  };

  const handleCarCreate = async (values) => {
    try {
      await onCarCreate({
        ...values,
        type: VEHICLE_TYPE.CAR,
      });
      form.resetFields();
      message.success("Car Created!");
    } catch (error) {
      message.error("Error occured! Failed to create car.");
    }
  };

  return (
    <Form
      {...formItemLayout}
      form={form}
      name="createCar"
      onFinish={handleCarCreate}
      scrollToFirstError
    >
      <Form.Item
        name="make"
        label="Make"
        rules={[{ required: true, message: "Make is required" }]}
      >
        <Input placeholder="Enter Make" />
      </Form.Item>
      <Form.Item
        name="model"
        label="Model"
        rules={[{ required: true, message: "Model is required" }]}
      >
        <Input placeholder="Enter Model" />
      </Form.Item>
      <Form.Item
        name="engine"
        label="Engine"
        rules={[{ required: true, message: "engine is required" }]}
      >
        <Input placeholder="Enter Engine" />
      </Form.Item>
      <Form.Item
        name="doors"
        label="Doors"
        rules={[
          { required: true, message: "Doors is required" },
          { min: 3, type: "number", message: "Must have at least 3 doors" },
        ]}
      >
        <InputNumber
          step={1}
          placeholder="Enter Doors"
          style={{ width: "100%" }}
        />
      </Form.Item>
      <Form.Item
        name="wheels"
        label="Wheels"
        rules={[
          { required: true, message: "Wheels is required" },
          { min: 4, type: "number", message: "Must have at least 4 wheels" },
        ]}
      >
        <InputNumber
          step={1}
          placeholder="Enter Doors"
          style={{ width: "100%" }}
        />
      </Form.Item>
      <Form.Item
        name="bodyType"
        label="Body Type"
        rules={[{ required: true, message: "Body type is required" }]}
      >
        <Select placeholder="Select Body Type">
          <Select.Option value={0}>Hatchback</Select.Option>
          <Select.Option value={1}>Sedan</Select.Option>
          <Select.Option value={2}>SUV</Select.Option>
        </Select>
      </Form.Item>
      <Form.Item {...tailFormItemLayout}>
        <Button type="primary" htmlType="submit" loading={saving}>
          Create Car
        </Button>
      </Form.Item>
    </Form>
  );
};

export default CreateCarForm;
