﻿using System;

namespace Data.Entity
{
    public abstract class Vehicle
    {
        public enum VehicleType
        {
            Car,
            Boat,
            Bike
        }

        public Guid Id { get; set; }

        public VehicleType Type { get; set; }

        public string Make { get; set; }

        public string Model { get; set; }
    }
}