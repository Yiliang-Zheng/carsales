﻿namespace Data.Entity
{
    public class Car:Vehicle
    {
        public enum CarType
        {
            Hatchback,
            Sedan,
            SUV
        }
        public string Engine { get; set; }

        public int Doors { get; set; }

        public int Wheels { get; set; }

        public CarType BodyType { get; set; }
    }
}