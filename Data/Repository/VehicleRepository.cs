﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.Entity;

namespace Data.Repository
{
    public class VehicleRepository: IRepository<Vehicle>
    {
        private readonly List<Vehicle> _vehicles;

        public VehicleRepository()
        {
            this._vehicles = new List<Vehicle>();
        }

        public Task<Vehicle> GetById(Guid id)
        {
            var result = this._vehicles.FirstOrDefault(p => p.Id == id);
            return Task.FromResult(result);
        }


        public Task<List<Vehicle>> List()
        {
            return Task.FromResult(this._vehicles);
        }

        public Task<List<Vehicle>> ListBy(Func<Vehicle, bool> predicate)
        {
            var result = this._vehicles.Where(predicate).ToList();
            return Task.FromResult(result);
        }

        public Task Update(Vehicle item)
        {
            var selected = this._vehicles.FirstOrDefault(p => p.Id == item.Id);
            if (selected == null) throw new Exception("Invalid vehicle. Vehicle not found.");

            this._vehicles.Remove(selected);
            this._vehicles.Add(item);
            return Task.FromResult<object>(null);
        }

        public Task Insert(Vehicle item)
        {
            this._vehicles.Add(item);
            return Task.FromResult<object>(null);
        }

        public Task Delete(Vehicle item)
        {
            var selected = this._vehicles.FirstOrDefault(p => p.Id == item.Id);
            if (selected == null) throw new Exception("Invalid vehicle. Vehicle not found.");

            this._vehicles.Remove(selected);
            return Task.FromResult<object>(null);
        }
    }
}