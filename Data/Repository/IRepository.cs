﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Data.Entity;

namespace Data.Repository
{
    public interface IRepository<T>
    {
        Task<T> GetById(Guid id);


        Task<List<T>> List();

        Task<List<T>> ListBy(Func<T, bool> predicate);

        Task Update(T item);

        Task Insert(T item);

        Task Delete(T item);
    }
}