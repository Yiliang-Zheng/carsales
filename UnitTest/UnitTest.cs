using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Data.Entity;
using Data.Repository;
using FluentValidation;
using Moq;
using Web;
using Web.Model;
using Web.Services;
using Xunit;

namespace UnitTest
{
    public class UnitTest
    {
        private readonly IMapper _mapper;
        private readonly IValidator<CarViewModel> _validator;
        private readonly Vehicle _sampleCar;
        public UnitTest()
        {
            var profile = new AutoMapperProfile();
            var config = new MapperConfiguration(p=>p.AddProfile(profile));
            this._mapper = new Mapper(config);
            this._validator = new CarViewModelValidator();
            this._sampleCar = new Car
            {
                Id = Guid.NewGuid(),
                Make = "Tesla",
                Model = "Model-X",
                Engine = "Long Performance",
                Doors = 5,
                Type = Vehicle.VehicleType.Car,
                Wheels = 4,
                BodyType = Car.CarType.SUV
            };
        }
        [Fact]
        public async Task CarService_ListVehicles_return_expected_result()
        {
            //arrange
            var mockVehicleRepo = new Mock<IRepository<Vehicle>>();
            mockVehicleRepo.Setup(p => p.ListBy(It.IsAny<Func<Vehicle, bool>>()))
                .ReturnsAsync(new List<Vehicle>()
                {
                    this._sampleCar
                });

            //act
            var carService = new CarService(mockVehicleRepo.Object, this._mapper, this._validator);
            var result = await carService.ListVehicles();

            //assert
            Assert.Single(result);
            Assert.Collection(result, item=>Assert.Contains("Tesla", item.Make));
        }

        [Fact]
        public async Task CarService_CreateVehicle_should_throw_validation_exception()
        {
            //arrange
            var mockVehicleRepo = new Mock<IRepository<Vehicle>>();
            mockVehicleRepo.Setup(p => p.Insert(It.IsAny<Vehicle>()))
                .Returns(Task.CompletedTask);

            var car = new CarViewModel()
            {
                Id = Guid.NewGuid(),
                Make = "Tesla",
                Model = "Model-X",
                Engine = "Long Performance",
                Doors = 2,
                Type = Vehicle.VehicleType.Car,
                Wheels = 1,
                BodyType = (int)Car.CarType.SUV
            };

            //act
            var carService =new CarService(mockVehicleRepo.Object, this._mapper, this._validator);
            Task Act() => carService.CreateVehicle(car);

            //assert
            await Assert.ThrowsAsync<ValidationException>(Act);
            

        }

        [Fact]
        public async Task CarService_CreateVehicle_should_create_car()
        {
            //arrange
            var mockVehicleList = new List<Vehicle>();
            var mockVehicleRepo = new Mock<IRepository<Vehicle>>();
            mockVehicleRepo.Setup(p => p.Insert(It.IsAny<Vehicle>()))
                .Callback((Vehicle item)=> mockVehicleList.Add(item) )
                .Returns(Task.CompletedTask);
            mockVehicleRepo.Setup(p => p.ListBy(It.IsAny<Func<Vehicle, bool>>()));

            var car = new CarViewModel()
            {
                Id = Guid.NewGuid(),
                Make = "Tesla",
                Model = "Model-X",
                Engine = "Long Performance",
                Doors = 5,
                Type = Vehicle.VehicleType.Car,
                Wheels = 4,
                BodyType = (int)Car.CarType.SUV
            };

            //act
            var carService = new CarService(mockVehicleRepo.Object, this._mapper, this._validator);
            await carService.CreateVehicle(car);
            //assert
            Assert.Single(mockVehicleList);
            Assert.Contains(mockVehicleList, p => p.Id == car.Id);
        }

        [Fact]
        public async Task VehicleRepo_GetById_should_return_null()
        {
            //arrange
            var repo = new VehicleRepository();
            //act 
            var result = await repo.GetById(Guid.NewGuid());

            //assert
            Assert.Null(result);
        }

        [Fact]
        public async Task VehicleRepo_List_should_return_empty()
        {
            //arrange
            var repo = new VehicleRepository();
            //act
            var result = await repo.List();
            //assert
            Assert.Empty(result);
        }

        [Fact]
        public async Task VehicleRepo_ListBy_should_return_empty()
        {
            //arrange
            var repo = new VehicleRepository();
            //act
            var result = await repo.ListBy(p => p.Type == Vehicle.VehicleType.Car);
            //assert
            Assert.Empty(result);
        }

        [Fact]
        public async Task VehicleRepo_Insert_should_create_car()
        {
            //arrange
            var repo = new VehicleRepository();
            //act
            await repo.Insert(this._sampleCar);
            var list = await repo.List();
            //assert
            Assert.Single(list);
            Assert.Contains(list, p => p.Make == "Tesla");
        }

        [Fact]
        public async Task VehicleRepo_Update_should_throw_exception()
        {
            //arrange
            var repo = new VehicleRepository();
            //act
            Task Act() => repo.Update(this._sampleCar);

            //assert
            var exception =  await Assert.ThrowsAsync<Exception>(Act);
            Assert.Equal("Invalid vehicle. Vehicle not found.", exception.Message);
        }

        [Fact]
        public async Task VehicleRepo_Update_should_update_car()
        {
            //arrange
            var repo = new VehicleRepository();
            //act
            await repo.Insert(this._sampleCar);
            var updatedCar = new Car
            {
                Id = this._sampleCar.Id,
                Make = "Tesla",
                Model = "Model 3",
                Engine = "Standard",
                Type = Vehicle.VehicleType.Car,
                BodyType = Car.CarType.Sedan,
                Wheels = 4,
                Doors = 5
            };
            await repo.Update(updatedCar);
            //assert
            var result = (Car)await repo.GetById(this._sampleCar.Id);
            Assert.NotNull(result);
            Assert.Equal("Model 3", result.Model);
            Assert.Equal("Standard", result.Engine);
            Assert.Equal(Car.CarType.Sedan, result.BodyType);
        }
    }
}
